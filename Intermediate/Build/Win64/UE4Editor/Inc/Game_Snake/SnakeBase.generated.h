// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAME_SNAKE_SnakeBase_generated_h
#error "SnakeBase.generated.h already included, missing '#pragma once' in SnakeBase.h"
#endif
#define GAME_SNAKE_SnakeBase_generated_h

#define Game_Snake_Source_Game_Snake_SnakeBase_h_24_SPARSE_DATA
#define Game_Snake_Source_Game_Snake_SnakeBase_h_24_RPC_WRAPPERS
#define Game_Snake_Source_Game_Snake_SnakeBase_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Game_Snake_Source_Game_Snake_SnakeBase_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeBase(); \
	friend struct Z_Construct_UClass_ASnakeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Game_Snake"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBase)


#define Game_Snake_Source_Game_Snake_SnakeBase_h_24_INCLASS \
private: \
	static void StaticRegisterNativesASnakeBase(); \
	friend struct Z_Construct_UClass_ASnakeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Game_Snake"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBase)


#define Game_Snake_Source_Game_Snake_SnakeBase_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBase(ASnakeBase&&); \
	NO_API ASnakeBase(const ASnakeBase&); \
public:


#define Game_Snake_Source_Game_Snake_SnakeBase_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBase(ASnakeBase&&); \
	NO_API ASnakeBase(const ASnakeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeBase)


#define Game_Snake_Source_Game_Snake_SnakeBase_h_24_PRIVATE_PROPERTY_OFFSET
#define Game_Snake_Source_Game_Snake_SnakeBase_h_21_PROLOG
#define Game_Snake_Source_Game_Snake_SnakeBase_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Game_Snake_Source_Game_Snake_SnakeBase_h_24_PRIVATE_PROPERTY_OFFSET \
	Game_Snake_Source_Game_Snake_SnakeBase_h_24_SPARSE_DATA \
	Game_Snake_Source_Game_Snake_SnakeBase_h_24_RPC_WRAPPERS \
	Game_Snake_Source_Game_Snake_SnakeBase_h_24_INCLASS \
	Game_Snake_Source_Game_Snake_SnakeBase_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Game_Snake_Source_Game_Snake_SnakeBase_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Game_Snake_Source_Game_Snake_SnakeBase_h_24_PRIVATE_PROPERTY_OFFSET \
	Game_Snake_Source_Game_Snake_SnakeBase_h_24_SPARSE_DATA \
	Game_Snake_Source_Game_Snake_SnakeBase_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Game_Snake_Source_Game_Snake_SnakeBase_h_24_INCLASS_NO_PURE_DECLS \
	Game_Snake_Source_Game_Snake_SnakeBase_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAME_SNAKE_API UClass* StaticClass<class ASnakeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Game_Snake_Source_Game_Snake_SnakeBase_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::LEFT) \
	op(EMovementDirection::RIGHT) 

enum class EMovementDirection;
template<> GAME_SNAKE_API UEnum* StaticEnum<EMovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
