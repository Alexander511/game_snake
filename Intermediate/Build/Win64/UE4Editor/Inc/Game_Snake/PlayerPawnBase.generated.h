// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAME_SNAKE_PlayerPawnBase_generated_h
#error "PlayerPawnBase.generated.h already included, missing '#pragma once' in PlayerPawnBase.h"
#endif
#define GAME_SNAKE_PlayerPawnBase_generated_h

#define Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_SPARSE_DATA
#define Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandlePlayerHorizontalInput(Z_Param_value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandlePlayerVerticalInput(Z_Param_value); \
		P_NATIVE_END; \
	}


#define Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandlePlayerHorizontalInput(Z_Param_value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandlePlayerVerticalInput(Z_Param_value); \
		P_NATIVE_END; \
	}


#define Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerPawnBase(); \
	friend struct Z_Construct_UClass_APlayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APlayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Game_Snake"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawnBase)


#define Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerPawnBase(); \
	friend struct Z_Construct_UClass_APlayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APlayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Game_Snake"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawnBase)


#define Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerPawnBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerPawnBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawnBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawnBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawnBase(APlayerPawnBase&&); \
	NO_API APlayerPawnBase(const APlayerPawnBase&); \
public:


#define Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawnBase(APlayerPawnBase&&); \
	NO_API APlayerPawnBase(const APlayerPawnBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawnBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawnBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerPawnBase)


#define Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Game_Snake_Source_Game_Snake_PlayerPawnBase_h_12_PROLOG
#define Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_SPARSE_DATA \
	Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_RPC_WRAPPERS \
	Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_INCLASS \
	Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_SPARSE_DATA \
	Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
	Game_Snake_Source_Game_Snake_PlayerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAME_SNAKE_API UClass* StaticClass<class APlayerPawnBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Game_Snake_Source_Game_Snake_PlayerPawnBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
