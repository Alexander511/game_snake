// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAME_SNAKE_Game_SnakeGameModeBase_generated_h
#error "Game_SnakeGameModeBase.generated.h already included, missing '#pragma once' in Game_SnakeGameModeBase.h"
#endif
#define GAME_SNAKE_Game_SnakeGameModeBase_generated_h

#define Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_SPARSE_DATA
#define Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_RPC_WRAPPERS
#define Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGame_SnakeGameModeBase(); \
	friend struct Z_Construct_UClass_AGame_SnakeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AGame_SnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Game_Snake"), NO_API) \
	DECLARE_SERIALIZER(AGame_SnakeGameModeBase)


#define Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAGame_SnakeGameModeBase(); \
	friend struct Z_Construct_UClass_AGame_SnakeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AGame_SnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Game_Snake"), NO_API) \
	DECLARE_SERIALIZER(AGame_SnakeGameModeBase)


#define Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGame_SnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGame_SnakeGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGame_SnakeGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGame_SnakeGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGame_SnakeGameModeBase(AGame_SnakeGameModeBase&&); \
	NO_API AGame_SnakeGameModeBase(const AGame_SnakeGameModeBase&); \
public:


#define Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGame_SnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGame_SnakeGameModeBase(AGame_SnakeGameModeBase&&); \
	NO_API AGame_SnakeGameModeBase(const AGame_SnakeGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGame_SnakeGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGame_SnakeGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGame_SnakeGameModeBase)


#define Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_12_PROLOG
#define Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_SPARSE_DATA \
	Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_RPC_WRAPPERS \
	Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_INCLASS \
	Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_SPARSE_DATA \
	Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAME_SNAKE_API UClass* StaticClass<class AGame_SnakeGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Game_Snake_Source_Game_Snake_Game_SnakeGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
