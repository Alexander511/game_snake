// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Game_SnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GAME_SNAKE_API AGame_SnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
